<?php

namespace Test\JLanger\Di\Loader;

use JLanger\Di\Loader\DelegatingLoader;
use JLanger\Di\Loader\ServiceConfigurationLoaderInterface;
use JLanger\Di\ServiceConfiguration;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DelegatingLoaderTest extends TestCase
{
    private DelegatingLoader $delegatingLoader;

    /**
     * @var ServiceConfigurationLoaderInterface|MockObject
     */
    private $loader1;

    /**
     * @var ServiceConfigurationLoaderInterface|MockObject
     */
    private $loader2;

    protected function setUp(): void
    {
        $this->loader1 = $this->createMock(ServiceConfigurationLoaderInterface::class);
        $this->loader2 = $this->createMock(ServiceConfigurationLoaderInterface::class);

        $this->delegatingLoader = new DelegatingLoader(
            [
                $this->loader1,
                $this->loader2,
            ]
        );
    }

    public function testDelegatingLoadingContent(): void
    {
        $content = '{ "test": {"classFQN": "Testing"}}';
        $this->loader1
            ->expects(self::once())
            ->method('load')
            ->with($content)
            ->willReturn(
                [
                'service' => new ServiceConfiguration('Testing', 'service', []),
                ]
            );

        $this->loader2
            ->expects(self::once())
            ->method('load')
            ->with($content)
            ->willReturn([]);

        self::assertEquals(
            $this->delegatingLoader->load($content),
            [
                'service' => new ServiceConfiguration('Testing', 'service', []),
            ]
        );
    }

    public function testDelegatingLoadingFiles(): void
    {
        $content = 'some_file.xml';
        $this->loader1
            ->expects(self::once())
            ->method('load')
            ->with($content)
            ->willReturn(
                [
                'service' => new ServiceConfiguration('Testing', 'service', []),
                ]
            );

        $this->loader2
            ->expects(self::once())
            ->method('load')
            ->with($content)
            ->willReturn([]);

        self::assertEquals(
            $this->delegatingLoader->load($content),
            [
                'service' => new ServiceConfiguration('Testing', 'service', []),
            ]
        );
    }

    public function testLastLoaderOverrides(): void
    {
        $content = 'some_file.xml';
        $this->loader1
            ->expects(self::once())
            ->method('load')
            ->with($content)
            ->willReturn(
                [
                'service' => new ServiceConfiguration('Testing', 'service', []),
                ]
            );

        $this->loader2
            ->expects(self::once())
            ->method('load')
            ->with($content)
            ->willReturn(
                [
                'service' => new ServiceConfiguration('SomeService', 'service', []),
                ]
            );

        self::assertEquals(
            $this->delegatingLoader->load($content),
            [
                'service' => new ServiceConfiguration('SomeService', 'service', []),
            ]
        );
    }
}
