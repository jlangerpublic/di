<?php

namespace Test\JLanger\Di\Loader;

use JLanger\Di\Loader\XMLLoader;
use JLanger\Di\ServiceConfiguration;
use PHPUnit\Framework\TestCase;
use Test\JLanger\Di\TestClass;

class XMLLoaderTest extends TestCase
{
    private XMLLoader $xmlLoader;

    protected function setUp(): void
    {
        $this->xmlLoader = new XMLLoader();
    }

    public function testLoadingStringWithMissingService(): void
    {
        $this->expectExceptionMessage('There has to be one main xml node named "services"');

        $this->xmlLoader->load('<hello></hello>');
    }

    public function testLoadingString(): void
    {
        $container = $this->xmlLoader->load(
            '<services>
            <service name="service_name" classFQN="SomeClass"></service>
        </services>'
        );
        
        self::assertEquals(
            $container, [
            new ServiceConfiguration('SomeClass', 'service_name', [])
            ]
        );
    }

    public function testLoadingFile(): void
    {
        self::assertEquals(
            $this->xmlLoader->loadFile(__DIR__.'/../data/loader/services.xml'), 
            [
                new ServiceConfiguration(TestClass::class, 'test', [])
            ]
        );
    }
    
    public function testLoadingFileWithArguments(): void
    {
        self::assertEquals(
            $this->xmlLoader->loadFile(__DIR__.'/../data/loader/services_with_args.xml'),
            [
                new ServiceConfiguration(
                    TestClass::class, 'test', [
                    'abc',
                    '@init_rt',
                    ]
                )
            ]
        );
    }

    public function testLoadingFileWithTwoServices(): void
    {
        self::assertEquals(
            $this->xmlLoader->loadFile(__DIR__.'/../data/loader/two_services.xml'),
            [
                new ServiceConfiguration(TestClass::class, 'test', []),
                new ServiceConfiguration('SomeClass', 'test2', ['abc']),
            ]
        );
    }
}
