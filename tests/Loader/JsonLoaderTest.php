<?php

namespace Test\JLanger\Di\Loader;

use JLanger\Di\Loader\JsonLoader;
use JLanger\Di\ServiceConfiguration;
use PHPUnit\Framework\TestCase;
use Test\JLanger\Di\TestClass;

class JsonLoaderTest extends TestCase
{
    private JsonLoader $jsonLoader;

    protected function setUp(): void
    {
        $this->jsonLoader = new JsonLoader();
    }
    
    public function testParsingInvalidJson(): void
    {
        self::assertSame($this->jsonLoader->load('abc'), []);
    }

    public function testLoadingString(): void
    {
        $container = $this->jsonLoader->load(
            '
        {
            "service_name": {
                "classFQN": "SomeClass"
            }
        }'
        );
        
        self::assertEquals(
            $container, [
            new ServiceConfiguration('SomeClass', 'service_name', [])
            ]
        );
    }

    public function testLoadingFile(): void
    {
        self::assertEquals(
            $this->jsonLoader->loadFile(__DIR__.'/../data/loader/services.json'), 
            [
                new ServiceConfiguration(TestClass::class, 'test', [])
            ]
        );
    }
    
    public function testLoadingFileWithArguments(): void
    {
        self::assertEquals(
            $this->jsonLoader->loadFile(__DIR__.'/../data/loader/services_with_args.json'),
            [
                new ServiceConfiguration(
                    TestClass::class, 'test', [
                    'abc',
                    '@init_rt',
                    ]
                )
            ]
        );
    }

    public function testLoadingFileWithTwoServices(): void
    {
        self::assertEquals(
            $this->jsonLoader->loadFile(__DIR__.'/../data/loader/two_services.json'),
            [
                new ServiceConfiguration(TestClass::class, 'test', []),
                new ServiceConfiguration('SomeClass', 'test2', ['abc']),
            ]
        );
    }
}
