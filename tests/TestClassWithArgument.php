<?php

namespace Test\JLanger\Di;

use JLanger\Di\Container;

class TestClassWithArgument
{
    public function __construct(Container $container) { }
}
