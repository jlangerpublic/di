<?php

namespace Test\JLanger\Di;

use JLanger\Di\ServiceConfiguration;
use PHPUnit\Framework\TestCase;

class ServiceConfigurationTest extends TestCase
{
    public function testGetters(): void
    {
        $serviceConfiguration = new ServiceConfiguration(self::class, 'service_name', []);

        self::assertSame($serviceConfiguration->getFQDN(), self::class);
        self::assertSame($serviceConfiguration->getName(), 'service_name');
        self::assertSame($serviceConfiguration->getParams(), []);
    }

    /**
     * @param string      $serviceName
     * @param string|null $expectedName
     *
     * @dataProvider dataGettingTheServiceName
     */
    public function testGettingTheServiceName(string $serviceName, ?string $expectedName): void
    {
        self::assertSame(ServiceConfiguration::getServiceName($serviceName), $expectedName);
    }

    public function dataGettingTheServiceName(): array
    {
        return [
            'empty string is not a service' => ['', null],
            'Container is not a service' => ['container', null],
            'Service name' => ['@init_testing', 'testing'],
        ];
    }

    public function testGetChildObjectNames(): void
    {
        $serviceConfiguration = new ServiceConfiguration(
            self::class, 'service_name', [
            'string_param',
            '@init_testing',
            ['a', 'r', 'r', 'a', 'y'],
            1233,
            new \stdClass(),
            true,
            ]
        );

        self::assertSame($serviceConfiguration->getChildObjectNames(), ['testing']);
    }
}
