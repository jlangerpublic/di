<?php

namespace Test\JLanger\Di;

use JLanger\Di\Container;
use JLanger\Di\ServiceConfiguration;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    public function testContainerValidationFindingACircularReference(): void
    {
        $this->expectExceptionMessage('Circular dependency in service: arouter');

        new Container(
            [
                new ServiceConfiguration(TestClass::class, 'router', ['@init_arouter']),
                new ServiceConfiguration(TestClass::class, 'arouter', ['@init_router']),
            ]
        );
    }

    public function testErrorOnInvalidReference(): void
    {
        $this->expectExceptionMessage('No service defined with the name: service_that_does_not_exist');

        new Container(
            [
                new ServiceConfiguration(TestClass::class, 'router', ['@init_service_that_does_not_exist']),
            ]
        );
    }

    public function testContainerIsAlwaysValid(): void
    {
        $container = new Container([
                new ServiceConfiguration(TestClassWithArgument::class, 'some_service', ['@init_container']),
            ]
        );

        self::assertInstanceOf(TestClassWithArgument::class, $container->get('some_service'));
    }

    public function testItOnlyCreatesClassesOnce(): void
    {
        $subject = new class {
            public int $constructorCalls = 0;

            public function __construct()
            {
                $this->constructorCalls++;
            }
        };

        $container = new Container([
            new ServiceConfiguration(get_class($subject), 'some_service', []),
        ]);

        $container->get('some_service');
        $container->get('some_service');

        self::assertSame(1, $subject->constructorCalls);
    }
}
