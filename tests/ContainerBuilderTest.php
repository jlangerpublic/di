<?php
declare(strict_types=1);

namespace Test\JLanger\Di;

use JLanger\Di\Loader\ServiceConfigurationLoaderInterface;
use JLanger\Di\ServiceConfiguration;
use PHPUnit\Framework\TestCase;
use JLanger\Di\ContainerBuilder;

class ContainerBuilderTest extends TestCase
{
    private ContainerBuilder $containerBuilder;

    protected function setUp(): void
    {
        $this->containerBuilder = new ContainerBuilder();
    }

    public function testLoadingAString(): void
    {
        $this->containerBuilder->addString(
        '<services>
            <service name="test" classFQN="'.TestClass::class.'" />
        </services>'
        );

        $container = $this->containerBuilder->getContainer();
        self::assertInstanceOf(TestClass::class, $container->get('test'));
    }
    
    public function testThrowingAnErrorMessageWhenReadingFromString(): void
    {
        $this->expectExceptionMessage('Service Tag needs to have a class associated with@<string content>:2');

        $this->containerBuilder->addString(
            '<services>
            <service name="test" />
        </services>'
        );

        $container = $this->containerBuilder->getContainer();
        self::assertInstanceOf(TestClass::class, $container->get('test'));
    }

    public function testLoadingAJSONFile(): void
    {
        $this->containerBuilder->addFile(__DIR__.'/data/services.json');
        $container = $this->containerBuilder->getContainer();
        self::assertInstanceOf(TestClass::class, $container->get('test'));
    }

    public function testSettingACustomLoader(): void
    {
        $configurationLoader = $this->createMock(ServiceConfigurationLoaderInterface::class);
        $configurationLoader
            ->expects(self::once())
            ->method('loadFile')
            ->with('test_file.txt')
            ->willReturn(
                [
                'test' => new ServiceConfiguration(TestClass::class, 'test', []),
                ]
            );

        $this->containerBuilder->addFile('test_file.txt');
        $this->containerBuilder->setLoader($configurationLoader);

        $container = $this->containerBuilder->getContainer();
        self::assertInstanceOf(TestClass::class, $container->get('test'));
    }

    public function testLoadingAnXMLFile(): void
    {
        $this->containerBuilder->addFile(__DIR__.'/data/services.xml');
        $container = $this->containerBuilder->getContainer();
        self::assertInstanceOf(TestClass::class, $container->get('test'));
    }
    
    public function testCallingAConfigurationPass(): void
    {
        $pass = new class {
            public function __invoke(array &$something)
            {
                TestCase::assertEquals(
                    $something, [
                    new ServiceConfiguration(TestClass::class, 'test', [])
                    ]
                );
                $something['service'] = new ServiceConfiguration(TestClass::class, 'service', []);
            }
        };
        
        $this->containerBuilder->addFile(__DIR__.'/data/services.xml');
        $this->containerBuilder->addConfigurationPass($pass);
        
        $container = $this->containerBuilder->getContainer();
        self::assertInstanceOf(TestClass::class, $container->get('test'));
        self::assertInstanceOf(TestClass::class, $container->get('service'));
    }
    
}

