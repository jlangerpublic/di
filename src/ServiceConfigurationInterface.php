<?php
declare(strict_types=1);

namespace JLanger\Di;

interface ServiceConfigurationInterface
{
    /**
     * Gets the classname of the service
     * 
     * @return string
     */
    public function getFQDN(): string;
    public function getName(): string;
    
    /**
     * @return array<mixed> 
     */
    public function getParams(): array;
    
    /**
     * @return array<string> 
     */
    public function getChildObjectNames(): array;
}
