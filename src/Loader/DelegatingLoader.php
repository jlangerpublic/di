<?php
declare(strict_types=1);

namespace JLanger\Di\Loader;

use function array_merge;

class DelegatingLoader implements ServiceConfigurationLoaderInterface
{
    /**
     * @var array<ServiceConfigurationLoaderInterface> 
     */
    private array $loader;

    public function __construct(array $loader)
    {
        $this->loader = $loader;
    }

    public function load(string $content): array
    {
        $services = [];
        foreach ($this->loader as $loader) {
            $services[] = $loader->load($content);
        }
        
        return array_merge(...$services);
    }

    public function loadFile(string $filename): array
    {
        $services = [];
        foreach ($this->loader as $loader) {
            $services[] = $loader->loadFile($filename);
        }

        return array_merge(...$services);
    }
}
