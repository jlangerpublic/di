<?php

declare(strict_types=1);

namespace JLanger\Di\Loader;

use JLanger\Di\ServiceConfiguration;
use function file_get_contents;
use function json_decode;

class JsonLoader implements ServiceConfigurationLoaderInterface
{
    
    public function load(string $content): array
    {
        $content = json_decode($content);
        if($content === null) {
            // If we can not decode the content as JSON it probably isn't JSON.
            return [];
        }
        
        $services = [];
        foreach($content as $serviceId => $serviceConfiguration) {
            $services[] = new ServiceConfiguration(
                $serviceConfiguration->classFQN,
                $serviceId,
                $serviceConfiguration->params ?? []
            );
        }
        
        return $services;
    }

    public function loadFile(string $filename): array
    {
        $content = file_get_contents($filename);

        return $this->load($content);
    }
}
