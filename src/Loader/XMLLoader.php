<?php

declare(strict_types=1);

namespace JLanger\Di\Loader;

use DOMDocument;
use DOMElement;
use DOMNode;
use DOMAttr;
use InvalidArgumentException;
use JLanger\Di\ServiceConfiguration;
use function file_get_contents;
use function strtolower;
use function substr;

class XMLLoader implements ServiceConfigurationLoaderInterface
{
    private string $fileName;
    
    public function load(string $xml): array
    {
        $document = new DOMDocument('', 'utf-8');
        $document->loadXML($xml);
        
        $rootElement = $document->getElementsByTagName('services');
        if ($rootElement->length !== 1) {
            throw new InvalidArgumentException('There has to be one main xml node named "services".');
        }

        $services = [];
        foreach ($rootElement[0]->getElementsByTagName('service') as $child) {
            $services[] = $this->loadService($child);
        }
        return $services;
    }

    /**
     * @param string $filename
     *
     * @return array
     * @throws \Exception
     */
    public function loadFile(string $filename): array
    {
        $this->fileName = $filename;

        if (strtolower(substr($filename, -4)) !== '.xml') {
            // We only support reading xml files here.
            return [];
        }

        return $this->load(file_get_contents($filename));
    }
    
    
    private function loadService(DOMNode $serviceNode): ServiceConfiguration
    {
        /**
 * @var DOMAttr $nameAttribute 
*/
        $nameAttribute = $serviceNode->attributes->getNamedItem('name');
        if ($nameAttribute === null) {
            $this->throwError('Service Tag needs to have a service name', $serviceNode);
        }

        /**
 * @var DOMAttr $classNameAttribute 
*/
        $classNameAttribute = $serviceNode->attributes->getNamedItem('classFQN');
        if ($classNameAttribute === null) {
            $this->throwError('Service Tag needs to have a class associated with', $serviceNode);
        }

        $parameters = [];
        foreach ($serviceNode->childNodes as $childNode) {
            if (!$childNode instanceof DOMElement) {
                continue;
            }

            if (strtolower($childNode->tagName) !== 'arg') {
                $this->throwError('The service tag should only contain arg tags', $childNode);
            }

            $parameters[] = $childNode->nodeValue;
        }

        return new ServiceConfiguration(
            $classNameAttribute->value,
            $nameAttribute->value,
            $parameters
        );
    }

    private function throwError(string $message, DOMNode $element): void
    {
        $fileName = $this->fileName ?? '<string content>';
        throw new InvalidArgumentException($message.'@'.$fileName.':'.$element->getLineNo());
    }
}
