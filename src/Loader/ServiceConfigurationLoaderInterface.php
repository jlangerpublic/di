<?php

namespace JLanger\Di\Loader;

interface ServiceConfigurationLoaderInterface
{
    public function load(string $content): array;
    
    public function loadFile(string $filename): array;
}
