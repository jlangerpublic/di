<?php
declare(strict_types=1);

namespace JLanger\Di;

use function strpos;

class ServiceConfiguration implements ServiceConfigurationInterface
{
    /**
     * @var string 
     */
    public const INIT_PREFIX = '@init_';

    private string $fqdn;
    private string $name;
    private array $params;

    public function __construct(string $fqdn, string $name, array $params)
    {
        $this->fqdn = $fqdn;
        $this->name = $name;
        $this->params = $params;
    }

    public function getFQDN(): string
    {
        return $this->fqdn;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function getChildObjectNames(): array
    {
        $childObjects = [];
        foreach ($this->params as $param) {
            if (!is_string($param)) {
                continue;
            }
            $serviceName = self::getServiceName($param);
            if ($serviceName !== null) {
                $childObjects[] = $serviceName;
            }
        }

        return $childObjects;
    }

    public static function getServiceName(string $maybeServiceName): ?string
    {
        if (strpos($maybeServiceName, self::INIT_PREFIX) === 0) {
            return substr($maybeServiceName, strlen(self::INIT_PREFIX));
        }

        return null;
    }
}
