<?php
declare(strict_types=1);

namespace JLanger\Di;

use Exception;
use InvalidArgumentException;
use function array_key_exists;
use function array_keys;
use function class_exists;

class Container
{
    public const SELF_ALIAS = 'container';

    /**
     * @var array<string, ServiceConfigurationInterface> 
     */
    private array $config = [];

    /**
     * @var array<string, object> 
     */
    private array $initializedServices = [];

    /**
     * Container constructor.
     *
     * @param array<string, ServiceConfiguration> $config
     */
    public function __construct(array $config)
    {
        foreach ($config as $serviceConfiguration) {
            $this->config[$serviceConfiguration->getName()] = $serviceConfiguration;
        }
        $this->validateConfig();
    }

    public function add(ServiceConfigurationInterface $serviceDefinition): void
    {
        $this->config[$serviceDefinition->getName()] = $serviceDefinition;
        $this->validateConfig();
    }

    /**
     * Gets a service from the container and throws an exception if the service name does not exist
     *
     * @param string $serviceName
     *
     * @return object
     */
    public function get(string $serviceName): object
    {
        if ($serviceName === self::SELF_ALIAS) {
            return $this;
        }

        if (array_key_exists($serviceName, $this->initializedServices)) {
            return $this->initializedServices[$serviceName];
        }

        if (!array_key_exists($serviceName, $this->config)) {
            throw new InvalidArgumentException('Object '.$serviceName.' does not exist.');
        }

        $serviceConfig = $this->config[$serviceName];

        $classFQN = $serviceConfig->getFQDN();
        if (!class_exists($classFQN)) {
            throw new InvalidArgumentException('Class: "'.$classFQN.'" for service '.$serviceName.' does not exist.');
        }

        $this->initializedServices[$serviceName] = $this->initializeObject($serviceConfig);

        return $this->initializedServices[$serviceName];
    }

    /**
     * @param ServiceConfigurationInterface $serviceConfiguration
     *
     * @return object
     */
    private function initializeObject(ServiceConfigurationInterface $serviceConfiguration): object
    {
        $className = $serviceConfiguration->getFQDN();
        $parameters = $serviceConfiguration->getParams();

        foreach ($parameters as $key => $parameter) {
            $serviceName = ServiceConfiguration::getServiceName($parameter);
            if ($serviceName !== null) {
                $parameters[$key] = $this->get($serviceName);
            }
        }

        return new $className(...$parameters);
    }

    private function validateConfig(): void
    {
        foreach (array_keys($this->config) as $serviceName) {
            $this->iterateConfig($serviceName);
        }
    }

    private function iterateConfig(string $current, array $parents = []): void
    {
        // The container itself is always valid.
        if($current === self::SELF_ALIAS) {
            return;
        }
       
        if(!array_key_exists($current, $this->config)) {
            throw new InvalidArgumentException('No service defined with the name: '. $current);
        }
        
        $childObjectNames = $this->config[$current]->getChildObjectNames();
        foreach ($childObjectNames as $name) {
            if (in_array($name, $parents, true)) {
                throw new InvalidArgumentException('Circular dependency in service: '.$current);
            }

            $this->iterateConfig($name, [...$parents, $current]);
        }
    }
}
