<?php
declare(strict_types=1);


namespace JLanger\Di;

use JLanger\Di\Loader\DelegatingLoader;
use JLanger\Di\Loader\ServiceConfigurationLoaderInterface;
use JLanger\Di\Loader\XMLLoader;
use JLanger\Di\Loader\JsonLoader;
use function array_merge;

class ContainerBuilder
{
    private ServiceConfigurationLoaderInterface $configurationLoader;

    /**
     * @var array<string> 
     */
    private array $filesToLoad = [];
    
    private ?string $stringToLoad = null;
   
    /**
     * @var array<\Closure> 
     */
    private array $configurationPasses = [];

    public function __construct()
    {
        $this->configurationLoader = new DelegatingLoader(
            [
            new XMLLoader(),
            new JsonLoader(),
            ]
        );
    }

    public function setLoader(ServiceConfigurationLoaderInterface $loader): void
    {
        $this->configurationLoader = $loader;
    }

    public function addFile(string $file): void
    {
        $this->filesToLoad[] = $file;
    }
    
    public function addString(string $string): void
    {
        $this->stringToLoad = $string;
    }
    
    public function addConfigurationPass(callable $callable)
    {
        $this->configurationPasses[] = $callable;
    }

    public function getContainer(): Container
    {
        $configuration = array_merge(...array_map([$this->configurationLoader, 'loadFile'], $this->filesToLoad));
        if($this->stringToLoad !== null) {
            $stringConfig = $this->configurationLoader->load($this->stringToLoad);
            $configuration = array_merge($configuration, $stringConfig);
        }

        foreach ($this->configurationPasses as $configurationPass) {
            $configurationPass($configuration);
        }
        
        return new Container($configuration);
    }
    
}
